# Apps / Dienste zur Automatisierung und Analyse von ETA-Heizungen

Bei Fragen kontaktiert mich gern, bin noch nicht dazu gekommen gross zu dokumentieren.

## Fetcher

### [Repo](https://gitlab.com/txlab/eta-influx)

Speichert periodisch Heizungs-Werte in eine [InfluxDB](https://www.influxdata.com/)

![Screenshot Dashboard](https://gitlab.com/txlab/eta-mate/-/wikis/uploads/e4e2be95dd1bf1b363b2f90afa16dfb1/Screenshot_from_2021-10-19_14-27-16.png)

## Botter

### [Repo](https://gitlab.com/txlab/eta-bot)

Telegram Bot zur Koordination & Benachrichtigung bei benoetigtem Heizen, zur Koordinierung wer es macht, und zur Einsicht wie viel eine Ladung Holz den Pufferspeicher geladen hat.
Bei bedarf kann auch automatisch auf den Pellet-Modus umgeschaltet werden.

![Screenshot Heizvorgang](https://gitlab.com/txlab/eta-mate/-/wikis/uploads/10c9af6ba5f2a70cb1bcaa83b4be9d7a/Screenshot_from_2021-10-19_14-05-23.png)
![Screenshot Bereitschaft](https://gitlab.com/txlab/eta-mate/-/wikis/uploads/4dbd5b029eba64358f0052cf31d6c747/Screenshot_20211019-114523.jpg)
